

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TesttingCustomerClass.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class TesttingCustomerClass
{
    /**
     * Default constructor for test class TesttingCustomerClass
     */
    public TesttingCustomerClass()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void TestCustomersVipSeats()
    {
        Customers customer1 = new Customers("Hassan");
        Customers customer2 = new Customers("Hassan2");
        Customers customer3 = new Customers("Hassan3");
        Customers customer4 = new Customers("Hassan4");
        Customers customer5 = new Customers("Hassan5");
        
    }
        @Test
    public void TestCustomersStandardSeats()
    {   
        System.out.println("Testing the standard Seats");
        Customers customer6 = new Customers("Hassa6");
        Customers customer7 = new Customers("Hassan7");
        Customers customer8 = new Customers("Hassan8");
        Customers customer9 = new Customers("Hassan9");
        Customers customer10 = new Customers("Hassan10");
        Customers customer11 = new Customers("Hassan11");
        Customers customer12 = new Customers("Hassan12");
        Customers customer13 = new Customers("Hassan13");
        Customers customer14 = new Customers("Hassan14");
        Customers customer15 = new Customers("Hassan15");
        
    }
    @Test
    public void TestReports(){
        Staff StaffReport = new Staff();
        System.out.println("Printing the report of this Months report Higest income");
        StaffReport.ReportForThisMonthHigestIncome();
        System.out.println("Printing the report of Next Months report Higest income");
        StaffReport.ReportForNextMonthHigestIncome();
        System.out.println("");
        System.out.println("The average rating of each movie this month");
        StaffReport.ReportForThisMonthRating();
        System.out.println("The average rating of each movie next month");
        StaffReport.ReportForNextMonthHigestRating();
        
    }
}

